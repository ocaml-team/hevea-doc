\begin{tocenv}
\tocitem \@locref{usermanual}{\begin{@norefs}\@print{Part&#XA0;A}\quad{}\label{usermanual}Tutorial{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{getstarted}{\begin{@norefs}\@print{1}\quad{}How to get\label{getstarted} started{}\end{@norefs}}
\tocitem \@locref{sec4}{\begin{@norefs}\@print{2}\quad{}Style files{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec5}{\begin{@norefs}\@print{2.1}\quad{}Standard base styles{}\end{@norefs}}
\tocitem \@locref{sec6}{\begin{@norefs}\@print{2.2}\quad{}Other base styles{}\end{@norefs}}
\tocitem \@locref{sec7}{\begin{@norefs}\@print{2.3}\quad{}Other style files{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec10}{\begin{@norefs}\@print{3}\quad{}A note on style{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec11}{\begin{@norefs}\@print{3.1}\quad{}Spacing, Paragraphs{}\end{@norefs}}
\tocitem \@locref{sec14}{\begin{@norefs}\@print{3.2}\quad{}Math mode{}\end{@norefs}}
\tocitem \@locref{sec19}{\begin{@norefs}\@print{3.3}\quad{}Warnings{}\end{@norefs}}
\tocitem \@locref{sec20}{\begin{@norefs}\@print{3.4}\quad{}Commands{}\end{@norefs}}
\tocitem \@locref{sec21}{\begin{@norefs}\@print{3.5}\quad{}Style choices{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec22}{\begin{@norefs}\@print{4}\quad{}How to detect and correct errors{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec23}{\begin{@norefs}\@print{4.1}\quad{}\hevea{} does not know a macro{}\end{@norefs}}
\tocitem \@locref{sec24}{\begin{@norefs}\@print{4.2}\quad{}\hevea{} incorrectly interprets a macro{}\end{@norefs}}
\tocitem \@locref{sec25}{\begin{@norefs}\@print{4.3}\quad{}\hevea{} crashes{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{both}{\begin{@norefs}\@print{5}\quad{}\label{both}Making \hevea{} and \LaTeX{} both happy{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec30}{\begin{@norefs}\@print{5.1}\quad{}File loading{}\end{@norefs}}
\tocitem \@locref{heveastyle}{\begin{@norefs}\@print{5.2}\quad{}The \label{heveastyle}\protect\texttt{hevea} package{}\end{@norefs}}
\tocitem \@locref{sec35}{\begin{@norefs}\@print{5.3}\quad{}Comments{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{imagen}{\begin{@norefs}\@print{6}\quad{}\label{imagen}With a little help from \LaTeX{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{image:file}{\begin{@norefs}\@print{6.1}\quad{}The \textit{image}\label{image:file} file{}\end{@norefs}}
\tocitem \@locref{sec38}{\begin{@norefs}\@print{6.2}\quad{}A toy example{}\end{@norefs}}
\tocitem \@locref{sec39}{\begin{@norefs}\@print{6.3}\quad{}Including Postscript images{}\end{@norefs}}
\tocitem \@locref{sec40}{\begin{@norefs}\@print{6.4}\quad{}Using filters{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{hacha}{\begin{@norefs}\@print{7}\quad{}\label{hacha}Cutting your document into pieces with \hacha{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec42}{\begin{@norefs}\@print{7.1}\quad{}Simple usage{}\end{@norefs}}
\tocitem \@locref{sec43}{\begin{@norefs}\@print{7.2}\quad{}Advanced usage{}\end{@norefs}}
\tocitem \@locref{sec49}{\begin{@norefs}\@print{7.3}\quad{}More Advanced Usage{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec65}{\begin{@norefs}\@print{8}\quad{}Generating \html{} constructs{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec66}{\begin{@norefs}\@print{8.1}\quad{}High-Level Commands{}\end{@norefs}}
\tocitem \@locref{imgsrc}{\begin{@norefs}\@print{8.2}\quad{}More on included\label{imgsrc} images{}\end{@norefs}}
\tocitem \@locref{internal}{\begin{@norefs}\@print{8.3}\quad{}Internal \label{internal}macros{}\end{@norefs}}
\tocitem \@locref{rawhtml}{\begin{@norefs}\@print{8.4}\quad{}The \texttt{rawhtml}\label{rawhtml} environment{}\end{@norefs}}
\tocitem \@locref{sec73}{\begin{@norefs}\@print{8.5}\quad{}Examples{}\end{@norefs}}
\tocitem \@locref{encodings}{\begin{@norefs}\@print{8.6}\quad{}The \label{encodings}document charset{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{style:sheets}{\begin{@norefs}\@print{9}\quad{}Support\label{style:sheets}\index{style-sheets} for style sheets{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec76}{\begin{@norefs}\@print{9.1}\quad{}Overview{}\end{@norefs}}
\tocitem \@locref{css:change:all}{\begin{@norefs}\@print{9.2}\quad{}Changing \label{css:change:all}
the style of all instances of an environment{}\end{@norefs}}
\tocitem \@locref{css:change}{\begin{@norefs}\@print{9.3}\quad{}Changing \label{css:change}the style of some instances of an environment{}\end{@norefs}}
\tocitem \@locref{whatclass}{\begin{@norefs}\@print{9.4}\quad{}Which class affects\label{whatclass} what{}\end{@norefs}}
\tocitem \@locref{sec80}{\begin{@norefs}\@print{9.5}\quad{}A few examples{}\end{@norefs}}
\tocitem \@locref{sec84}{\begin{@norefs}\@print{9.6}\quad{}Miscellaneous{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec89}{\begin{@norefs}\@print{10}\quad{}Customising \hevea{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec90}{\begin{@norefs}\@print{10.1}\quad{}Simple changes{}\end{@norefs}}
\tocitem \@locref{sec91}{\begin{@norefs}\@print{10.2}\quad{}Changing defaults for type-styles{}\end{@norefs}}
\tocitem \@locref{sec92}{\begin{@norefs}\@print{10.3}\quad{}Changing the interface of a command{}\end{@norefs}}
\tocitem \@locref{sec93}{\begin{@norefs}\@print{10.4}\quad{}Checking the optional argument within a command{}\end{@norefs}}
\tocitem \@locref{sec94}{\begin{@norefs}\@print{10.5}\quad{}Changing the format of images{}\end{@norefs}}
\tocitem \@locref{sec95}{\begin{@norefs}\@print{10.6}\quad{}Storing images in a separate directory{}\end{@norefs}}
\tocitem \@locref{imagen-source}{\begin{@norefs}\@print{10.7}\quad{}Controlling\label{imagen-source} \texttt{imagen} from document source{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{alternative}{\begin{@norefs}\@print{11}\quad{}Other \label{alternative}output formats{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec98}{\begin{@norefs}\@print{11.1}\quad{}Text{}\end{@norefs}}
\tocitem \@locref{sec99}{\begin{@norefs}\@print{11.2}\quad{}Info{}\end{@norefs}}
\end{tocenv}
\end{tocenv}
\tocitem \@locref{referencemanual}{\begin{@norefs}\@print{Part&#XA0;B}\quad{}\label{referencemanual}Reference manual{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec101}{\begin{@norefs}\@print{B.1}\quad{}Commands and Environments{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec102}{\begin{@norefs}\@print{B.1.1}\quad{}Command Names and Arguments{}\end{@norefs}}
\tocitem \@locref{sec103}{\begin{@norefs}\@print{B.1.2}\quad{}Environments{}\end{@norefs}}
\tocitem \@locref{sec104}{\begin{@norefs}\@print{B.1.3}\quad{}Fragile Commands{}\end{@norefs}}
\tocitem \@locref{sec105}{\begin{@norefs}\@print{B.1.4}\quad{}Declarations{}\end{@norefs}}
\tocitem \@locref{sec106}{\begin{@norefs}\@print{B.1.5}\quad{}Invisible Commands{}\end{@norefs}}
\tocitem \@locref{sec107}{\begin{@norefs}\@print{B.1.6}\quad{}The \texttt{\char92\char92} Command{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec108}{\begin{@norefs}\@print{B.2}\quad{}The Structure of the Document{}\end{@norefs}}
\tocitem \@locref{sec109}{\begin{@norefs}\@print{B.3}\quad{}Sentences and Paragraphs{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec110}{\begin{@norefs}\@print{B.3.1}\quad{}Spacing{}\end{@norefs}}
\tocitem \@locref{sec111}{\begin{@norefs}\@print{B.3.2}\quad{}Paragraphs{}\end{@norefs}}
\tocitem \@locref{sec:footnotes}{\begin{@norefs}\@print{B.3.3}\quad{}Footnotes\label{sec:footnotes}{}\end{@norefs}}
\tocitem \@locref{accents}{\begin{@norefs}\@print{B.3.4}\quad{}Accents\label{accents} and special symbols{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec114}{\begin{@norefs}\@print{B.4}\quad{}Sectioning{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{section:section}{\begin{@norefs}\@print{B.4.1}\quad{}\label{section:section}Sectioning Commands{}\end{@norefs}}
\tocitem \@locref{appendix}{\begin{@norefs}\@print{B.4.2}\quad{}The \label{appendix}Appendix{}\end{@norefs}}
\tocitem \@locref{sec117}{\begin{@norefs}\@print{B.4.3}\quad{}Table of Contents{}\end{@norefs}}
\tocitem \ahrefloc{no:number}{Use \hacha{}}
\end{tocenv}
\tocitem \@locref{sec119}{\begin{@norefs}\@print{B.5}\quad{}Classes, Packages and Page Styles{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec120}{\begin{@norefs}\@print{B.5.1}\quad{}Document Class{}\end{@norefs}}
\tocitem \@locref{sec121}{\begin{@norefs}\@print{B.5.2}\quad{}Packages and Page Styles{}\end{@norefs}}
\tocitem \@locref{sec122}{\begin{@norefs}\@print{B.5.3}\quad{}The Title Page and Abstract{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec123}{\begin{@norefs}\@print{B.6}\quad{}Displayed Paragraphs{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec124}{\begin{@norefs}\@print{B.6.1}\quad{}Quotation and Verse{}\end{@norefs}}
\tocitem \@locref{sec125}{\begin{@norefs}\@print{B.6.2}\quad{}List-Making environments{}\end{@norefs}}
\tocitem \@locref{sec126}{\begin{@norefs}\@print{B.6.3}\quad{}The \protect\texttt{list} and \protect\texttt{trivlist}
environments{}\end{@norefs}}
\tocitem \@locref{sec127}{\begin{@norefs}\@print{B.6.4}\quad{}Verbatim{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec128}{\begin{@norefs}\@print{B.7}\quad{}Mathematical Formulae{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec129}{\begin{@norefs}\@print{B.7.1}\quad{}Math Mode Environment{}\end{@norefs}}
\tocitem \@locref{sec130}{\begin{@norefs}\@print{B.7.2}\quad{}Common Structures{}\end{@norefs}}
\tocitem \@locref{sec131}{\begin{@norefs}\@print{B.7.3}\quad{}Square Root{}\end{@norefs}}
\tocitem \@locref{sec132}{\begin{@norefs}\@print{B.7.4}\quad{}Unicode and mathematical symbols{}\end{@norefs}}
\tocitem \@locref{sec133}{\begin{@norefs}\@print{B.7.5}\quad{}Putting one thing above/below/inside{}\end{@norefs}}
\tocitem \@locref{sec134}{\begin{@norefs}\@print{B.7.6}\quad{}Math accents{}\end{@norefs}}
\tocitem \@locref{sec135}{\begin{@norefs}\@print{B.7.7}\quad{}Spacing{}\end{@norefs}}
\tocitem \@locref{sec136}{\begin{@norefs}\@print{B.7.8}\quad{}Changing Style{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec137}{\begin{@norefs}\@print{B.8}\quad{}Definitions, Numbering{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec138}{\begin{@norefs}\@print{B.8.1}\quad{}Defining Commands{}\end{@norefs}}
\tocitem \@locref{sec139}{\begin{@norefs}\@print{B.8.2}\quad{}Defining Environments{}\end{@norefs}}
\tocitem \@locref{sec140}{\begin{@norefs}\@print{B.8.3}\quad{}Theorem-like Environments{}\end{@norefs}}
\tocitem \@locref{sec141}{\begin{@norefs}\@print{B.8.4}\quad{}Numbering{}\end{@norefs}}
\tocitem \@locref{sec142}{\begin{@norefs}\@print{B.8.5}\quad{}The \texttt{ifthen} Package{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec143}{\begin{@norefs}\@print{B.9}\quad{}Figures, Tables, and Other Floating Bodies{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec144}{\begin{@norefs}\@print{B.9.1}\quad{}Figures And Tables{}\end{@norefs}}
\tocitem \@locref{sec145}{\begin{@norefs}\@print{B.9.2}\quad{}Footnotes{}\end{@norefs}}
\tocitem \@locref{sec146}{\begin{@norefs}\@print{B.9.3}\quad{}Marginal Notes{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec147}{\begin{@norefs}\@print{B.10}\quad{}Lining It Up in Columns{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec148}{\begin{@norefs}\@print{B.10.1}\quad{}The \protect\texttt{tabbing} Environment{}\end{@norefs}}
\tocitem \@locref{sec149}{\begin{@norefs}\@print{B.10.2}\quad{}The \texttt{array} and \texttt{tabular}
environments{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec150}{\begin{@norefs}\@print{B.11}\quad{}Moving Information Around{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{files}{\begin{@norefs}\@print{B.11.1}\quad{}\label{files}Files{}\end{@norefs}}
\tocitem \@locref{cross-reference}{\begin{@norefs}\@print{B.11.2}\quad{}Cross-References\label{cross-reference}\label{cross}{}\end{@norefs}}
\tocitem \@locref{sec153}{\begin{@norefs}\@print{B.11.3}\quad{}Bibliography and Citations{}\end{@norefs}}
\tocitem \@locref{sec155}{\begin{@norefs}\@print{B.11.4}\quad{}Splitting the Input{}\end{@norefs}}
\tocitem \@locref{sec156}{\begin{@norefs}\@print{B.11.5}\quad{}Index and Glossary{}\end{@norefs}}
\tocitem \@locref{sec157}{\begin{@norefs}\@print{B.11.6}\quad{}Terminal Input and Output{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec158}{\begin{@norefs}\@print{B.12}\quad{}Line and Page Breaking{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec159}{\begin{@norefs}\@print{B.12.1}\quad{}Line Breaking{}\end{@norefs}}
\tocitem \@locref{sec160}{\begin{@norefs}\@print{B.12.2}\quad{}Page Breaking{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec161}{\begin{@norefs}\@print{B.13}\quad{}Lengths, Spaces and Boxes{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec162}{\begin{@norefs}\@print{B.13.1}\quad{}Length{}\end{@norefs}}
\tocitem \@locref{sec163}{\begin{@norefs}\@print{B.13.2}\quad{}Space{}\end{@norefs}}
\tocitem \@locref{sec164}{\begin{@norefs}\@print{B.13.3}\quad{}Boxes{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec165}{\begin{@norefs}\@print{B.14}\quad{}Pictures and Colours{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec166}{\begin{@norefs}\@print{B.14.1}\quad{}The \texttt{picture} environment and the \texttt{graphics}
Package{}\end{@norefs}}
\tocitem \@locref{sec167}{\begin{@norefs}\@print{B.14.2}\quad{}The \texttt{color} Package{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec170}{\begin{@norefs}\@print{B.15}\quad{}Font Selection{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec171}{\begin{@norefs}\@print{B.15.1}\quad{}Changing the Type Style{}\end{@norefs}}
\tocitem \@locref{sec172}{\begin{@norefs}\@print{B.15.2}\quad{}Changing the Type Size{}\end{@norefs}}
\tocitem \@locref{sec173}{\begin{@norefs}\@print{B.15.3}\quad{}Special Symbols{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec174}{\begin{@norefs}\@print{B.16}\quad{}Extra Features{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec175}{\begin{@norefs}\@print{B.16.1}\quad{}\TeX{} macros{}\end{@norefs}}
\tocitem \@locref{sec181}{\begin{@norefs}\@print{B.16.2}\quad{}Command Definition inside Command Definition{}\end{@norefs}}
\tocitem \@locref{sec182}{\begin{@norefs}\@print{B.16.3}\quad{}Date and time{}\end{@norefs}}
\tocitem \@locref{fancysection}{\begin{@norefs}\@print{B.16.4}\quad{}Fancy \label{fancysection}sectioning commands{}\end{@norefs}}
\tocitem \@locref{winfonts}{\begin{@norefs}\@print{B.16.5}\quad{}Targeting \label{winfonts}Windows{}\end{@norefs}}
\tocitem \@locref{mathjax}{\begin{@norefs}\@print{B.16.6}\quad{}\textsf{MathJax} \label{mathjax}support{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec189}{\begin{@norefs}\@print{B.17}\quad{}Implemented Packages{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec190}{\begin{@norefs}\@print{B.17.1}\quad{}AMS compatibility{}\end{@norefs}}
\tocitem \@locref{sec191}{\begin{@norefs}\@print{B.17.2}\quad{}The \texttt{array} and \texttt{tabularx}
packages{}\end{@norefs}}
\tocitem \@locref{calc}{\begin{@norefs}\@print{B.17.3}\quad{}The \texttt{calc}\label{calc} package{}\end{@norefs}}
\tocitem \@locref{inputenc}{\begin{@norefs}\@print{B.17.4}\quad{}Specifying \label{inputenc}the document input encoding, the \texttt{inputenc} package{}\end{@norefs}}
\tocitem \@locref{sec194}{\begin{@norefs}\@print{B.17.5}\quad{}The \texttt{hyphenat} package{}\end{@norefs}}
\tocitem \@locref{sec195}{\begin{@norefs}\@print{B.17.6}\quad{}More symbols{}\end{@norefs}}
\tocitem \@locref{commentpack}{\begin{@norefs}\@print{B.17.7}\quad{}The \texttt{comment}\label{commentpack} package{}\end{@norefs}}
\tocitem \@locref{multind}{\begin{@norefs}\@print{B.17.8}\quad{}Multiple Indexes with the \texttt{index} and
\texttt{multind}\label{multind} packages{}\end{@norefs}}
\tocitem \@locref{sec198}{\begin{@norefs}\@print{B.17.9}\quad{}``Natural'' bibliographies, the \texttt{natbib} package {}\end{@norefs}}
\tocitem \@locref{sec199}{\begin{@norefs}\@print{B.17.10}\quad{}Multiple bibliographies{}\end{@norefs}}
\tocitem \@locref{sec202}{\begin{@norefs}\@print{B.17.11}\quad{}Support for \texttt{babel}{}\end{@norefs}}
\tocitem \@locref{urlpackage}{\begin{@norefs}\@print{B.17.12}\quad{}The \label{urlpackage}\texttt{url} package{}\end{@norefs}}
\tocitem \@locref{sec207}{\begin{@norefs}\@print{B.17.13}\quad{}Verbatim text: the \texttt{moreverb} and
\texttt{verbatim} packages{}\end{@norefs}}
\tocitem \@locref{listings:package}{\begin{@norefs}\@print{B.17.14}\quad{}Typesetting \label{listings:package}computer languages: the \texttt{listings} package{}\end{@norefs}}
\tocitem \@locref{sec209}{\begin{@norefs}\@print{B.17.15}\quad{}(Non-)Multi page tabular material{}\end{@norefs}}
\tocitem \@locref{mathpartir:package}{\begin{@norefs}\@print{B.17.16}\quad{}Typesetting inference rules: the
\label{mathpartir:package} \aname{mathpartir}{\texttt{mathpartir}} package{}\end{@norefs}}
\tocitem \@locref{sec215}{\begin{@norefs}\@print{B.17.17}\quad{}The \texttt{ifpdf} package{}\end{@norefs}}
\tocitem \@locref{sec216}{\begin{@norefs}\@print{B.17.18}\quad{}Typesetting Thai{}\end{@norefs}}
\tocitem \@locref{sec217}{\begin{@norefs}\@print{B.17.19}\quad{}Hanging paragraphs{}\end{@norefs}}
\tocitem \@locref{sec218}{\begin{@norefs}\@print{B.17.20}\quad{}The \texttt{cleveref} package{}\end{@norefs}}
\tocitem \@locref{sec219}{\begin{@norefs}\@print{B.17.21}\quad{}Other packages{}\end{@norefs}}
\end{tocenv}
\end{tocenv}
\tocitem \@locref{practical}{\begin{@norefs}\@print{Part&#XA0;C}\quad{}\label{practical}Practical information{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec221}{\begin{@norefs}\@print{C.1}\quad{}Usage{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec222}{\begin{@norefs}\@print{C.1.1}\quad{}\hevea{} usage{}\end{@norefs}}
\tocitem \@locref{sec227}{\begin{@norefs}\@print{C.1.2}\quad{}\hacha{} usage{}\end{@norefs}}
\tocitem \@locref{sec228}{\begin{@norefs}\@print{C.1.3}\quad{}\texttt{esponja} usage{}\end{@norefs}}
\tocitem \@locref{bibhva}{\begin{@norefs}\@print{C.1.4}\quad{}\texttt{bibhva}\label{bibhva} usage{}\end{@norefs}}
\tocitem \@locref{sec232}{\begin{@norefs}\@print{C.1.5}\quad{}\texttt{imagen} usage{}\end{@norefs}}
\tocitem \@locref{sec233}{\begin{@norefs}\@print{C.1.6}\quad{}Invoking \texttt{hevea}, \texttt{hacha} and \texttt{imagen}{}\end{@norefs}}
\tocitem \@locref{makefile}{\begin{@norefs}\@print{C.1.7}\quad{}Using \label{makefile}\texttt{make}{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{browser}{\begin{@norefs}\@print{C.2}\quad{}Browser \label{browser}configuration{}\end{@norefs}}
\tocitem \@locref{sec236}{\begin{@norefs}\@print{C.3}\quad{}Availability{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec237}{\begin{@norefs}\@print{C.3.1}\quad{}Internet stuff{}\end{@norefs}}
\tocitem \@locref{sec238}{\begin{@norefs}\@print{C.3.2}\quad{}Law{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec239}{\begin{@norefs}\@print{C.4}\quad{}Installation{}\end{@norefs}}
\begin{tocenv}
\tocitem \@locref{sec240}{\begin{@norefs}\@print{C.4.1}\quad{}Requirements{}\end{@norefs}}
\tocitem \@locref{sec241}{\begin{@norefs}\@print{C.4.2}\quad{}Principles{}\end{@norefs}}
\end{tocenv}
\tocitem \@locref{sec242}{\begin{@norefs}\@print{C.5}\quad{}Other \LaTeX{} to \html{} translators{}\end{@norefs}}
\tocitem \@locref{sec243}{\begin{@norefs}\@print{C.6}\quad{}Acknowledgements{}\end{@norefs}}
\tocitem \ahrefloc{@biblio}{\refname}
\tocitem \ahrefloc{@index}{Index}
\end{tocenv}
\end{tocenv}

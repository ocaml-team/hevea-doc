hevea-doc (2.36-2) UNRELEASED; urgency=medium

  * Use secure URI in Homepage field.
  * Update standards version to 4.6.2, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 18 Sep 2022 16:30:50 -0000

hevea-doc (2.36-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version 4.6.1 (no change)

 -- Ralf Treinen <treinen@debian.org>  Sun, 18 Sep 2022 17:16:55 +0200

hevea-doc (2.35-1) unstable; urgency=medium

  * New upstream version
  * debian/watch: version 4 (no change)
  * Standards-version 4.6.0 (no change)
  * Debhelper compatibility level 13 (no change)
  * Set rules-requires-root=no

 -- Ralf Treinen <treinen@debian.org>  Sun, 17 Oct 2021 21:17:01 +0200

hevea-doc (2.34-1) unstable; urgency=medium

  * New upstream version.
  * Build-depend on debian-compat, remove file debian/compat
  * Debhelper compatibility level 12 (no change)
  * Standards-Version 4.5.0 (no change)

 -- Ralf Treinen <treinen@debian.org>  Wed, 08 Apr 2020 16:19:29 +0200

hevea-doc (2.32-1) unstable; urgency=medium

  * New upstream version.
  * Debhelper compatibility level 11
  * d/hevea-doc.install: drop *.gif (there aren't any), add *.svg
  * Updated Vcs-* to salsa
  * Privacy breach due to https request in html documents:
    - replace by file: url of local copy of mathjax
    - add dependency on libjs-mathjax
  * Standards-Version 4.2.0:
    - use https in format specification in d /copyright

 -- Ralf Treinen <treinen@debian.org>  Wed, 08 Aug 2018 17:00:49 +0200

hevea-doc (2.29-2) unstable; urgency=medium

  * upload to unstable with binary package

 -- Ralf Treinen <treinen@debian.org>  Sun, 25 Sep 2016 12:32:00 +0200

hevea-doc (2.29-1) unstable; urgency=medium

  [Rémi Vanicat]
  * removed Rémi Vanicat from uploaders.

  [Ralf Treinen]
  * new upstream release
  * Standards-Version 3.9.8 (no change)
  * update VCS-Git field

 -- Ralf Treinen <treinen@debian.org>  Fri, 23 Sep 2016 20:48:21 +0200

hevea-doc (2.28-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version 3.9.7 (no change)

 -- Ralf Treinen <treinen@debian.org>  Fri, 23 Sep 2016 20:36:50 +0200

hevea-doc (2.23-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version 3.9.6 (no change)
  * d/control: Canonise Vcs-fields

 -- Ralf Treinen <treinen@debian.org>  Fri, 29 May 2015 19:57:35 +0200

hevea-doc (2.18-1) unstable; urgency=low

  * New upstream version.

 -- Ralf Treinen <treinen@debian.org>  Sun, 05 Oct 2014 21:14:49 +0200

hevea-doc (2.16-1) unstable; urgency=medium

  * New upstream version.

 -- Ralf Treinen <treinen@debian.org>  Thu, 19 Jun 2014 20:52:16 +0200

hevea-doc (2.09-1) unstable; urgency=low

  * New upstream version.
  * Standards-Version 3.9.5 (no change)

 -- Ralf Treinen <treinen@debian.org>  Tue, 03 Dec 2013 08:25:56 +0100

hevea-doc (2.06-2) unstable; urgency=low

  * upload to unstable.

 -- Ralf Treinen <treinen@debian.org>  Mon, 06 May 2013 21:13:49 +0200

hevea-doc (2.06-1) experimental; urgency=low

  * New upstream version.

 -- Ralf Treinen <treinen@debian.org>  Tue, 23 Apr 2013 08:44:35 +0200

hevea-doc (2.02-1) experimental; urgency=low

  * New upstream release.
  * Standards-Version 3.9.4 (no change)

 -- Ralf Treinen <treinen@debian.org>  Thu, 31 Jan 2013 09:30:47 +0100

hevea-doc (2.00-1) experimental; urgency=low

  [ Stefano Zacchiroli ]
  * removes myself from Uploaders

  [ Sylvain Le Gall ]
  * Remove Sylvain Le Gall from uploaders

  [ Ralf Treinen ]
  * New upstream release.
  * Standards-Version 3.9.3
    - debian/copyright: migrate to machine-readable format 1.0
  * migrate to dh(1):
    - rewrite debian/rules
    - add debian/hevea-doc/{links,dirs,install}
    - debian/control: bump version in build-dependency on debhelper

 -- Ralf Treinen <treinen@debian.org>  Thu, 04 Oct 2012 20:07:48 +0200

hevea-doc (1.10-3) unstable; urgency=low

  [ Ralf Treinen ]
  * debian/control: Added Homepage field.
  * Add dependency on ${misc:Depends}
  * DH compatibility level 7
    - debian/rules: dh_clean-k => dh_prep
  * Standards-Version 3.8.3 (no change)
  * Changed doc-base section to Typesetting
  * debian/copyright: added copyright of INRIA
  * debian source format 3.0 (quilt)

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

  [ Stephane Glondu ]
  * Switch packaging to git

 -- Ralf Treinen <treinen@debian.org>  Sun, 03 Jan 2010 09:41:19 +0100

hevea-doc (1.10-2) unstable; urgency=low

  * Upload to unstable.
  * Setting Maintainer to debian-ocaml-maint.

 -- Ralf Treinen <treinen@debian.org>  Thu, 30 Aug 2007 20:51:21 +0200

hevea-doc (1.10-1) experimental; urgency=low

  * Fixed url in debian/watch.
  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Tue, 28 Aug 2007 09:50:36 +0200

hevea-doc (1.09-1) unstable; urgency=low

  * New upstream release.
  * Build-depends on debhelper instead of build-depends-indep.
  * Bumped Standards-Version to 3.7.2 (no change).

 -- Ralf Treinen <treinen@debian.org>  Sat, 11 Nov 2006 15:33:28 +0100

hevea-doc (1.08-1) unstable; urgency=low

  * New upsteam release.
  * Added debian/watch.
  * Bumped up dependency on debhelper to >= 4.0.

 -- Ralf Treinen <treinen@debian.org>  Sun,  5 Jun 2005 19:13:20 +0200

hevea-doc (1.07-1) unstable; urgency=low

  * New upstream release.
  * Upstream now ships fddl.html. Remove it from the diff.
  * Install html files in /usr/share/doc/hevea/html, symlink from
    /usr/share/doc/hevea-doc/html (closes: Bug#214586).

 -- Ralf Treinen <treinen@debian.org>  Thu,  9 Oct 2003 20:50:14 +0200

hevea-doc (1.06-2) unstable; urgency=low

  * Standards-Version 3.6.1.
  * File debian/compat instead of DH_COMPAT variable.
  * debhelper compatibility level 4.
  * More detailed long description (closes: Bug#209535).

 -- Ralf Treinen <treinen@debian.org>  Mon, 22 Sep 2003 21:54:38 +0200

hevea-doc (1.06-1) unstable; urgency=low

  * Initial Release (closes: Bug#147335). This has to go into non-free
    since the copyright heavily restricts modifications.
  * Added the licence file fddl.html as downloaded from the hevea home page
    at http://pauillac.inria.fr/~maranget/hevea/doc/fddl.html since
    upstream forgot to put it into the tarball.

 -- Ralf Treinen <treinen@debian.org>  Sat, 18 May 2002 10:26:43 +0200

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<meta name="generator" content="hevea 2.36">
<meta name="Author" content="Luc Maranget">
<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script><link rel="stylesheet" type="text/css" href="manual.css">
<title>How to detect and correct errors</title>
</head>
<body>
<a href="manual005.html"><img src="previous_motif.svg" alt="Previous"></a>
<a href="manual002.html"><img src="contents_motif.svg" alt="Up"></a>
<a href="manual007.html"><img src="next_motif.svg" alt="Next"></a>
<hr>
<h2 class="section" id="sec22">4&#X2003;How to detect and correct errors</h2>
<ul>
<li><a href="manual006.html#sec23">H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A does not know a macro</a>
</li><li><a href="manual006.html#sec24">H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A incorrectly interprets a macro</a>
</li><li><a href="manual006.html#sec25">H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A crashes</a>
</li></ul>
<p><a id="trouble"></a></p><p>Most of the problems that occur during the translation of a given
L<sup>A</sup>T<sub>E</sub>X file (say <code class="verb">trouble.tex</code>) can be detected and solved at
the macro-level. That is, most problems induce a macro-related warning
and can be solved by writing a few
macros. The best place for these macros is an user style file (say
<span class="c017">trouble.hva</span>) given as
argument to H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A.
</p><pre class="verbatim"># hevea trouble.hva trouble.tex
</pre><p>
By doing so, the macros written specially for H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A are not
seen by L<sup>A</sup>T<sub>E</sub>X. Even better, <code class="verb">trouble.tex</code> is not changed
at all.</p><p>A worth-mentiong alternative is inserting <code class="verb">\usepackage{trouble}</code>
in the document preamble. Then, given H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A semantics for
<code class="verb">\usepackage</code> (see Section&#XA0;<a href="manual027.html#usepackage">B.5.2</a>),
H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A-specific commands should be placed in
the file &#X201C;<span class="c017">trouble.hva</span>&#X201D; file, while L<sup>A</sup>T<sub>E</sub>X-specific commands
should be placed in the file &#X201C;<span class="c017">trouble.sty</span>&#X201D;.</p><p>Of course, adapting a document to H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A processing
will be easier if the L<sup>A</sup>T<sub>E</sub>X source is written in a
generic style, using macros.
Note that this style is recommended anyway, since it facilitates
document maintenance.</p>
<h3 class="subsection" id="sec23">4.1&#X2003;H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A does not know a macro</h3>
<p><a id="dontknow"></a>
</p><blockquote class="quote"><em>This section has been outdated by the implementation of
</em><code class="verb"><em>\raisebox</em></code><em> by C.&#XA0;Spiel. Nevertheless, the method illustrated here is still highly valuable. This method can be summarised as &#X201C;if H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A does not know about a macro, you may solve the problem a simple one yourself&#X201D; and &#X201C;do not ignore warnings&#X201D;.
</em></blockquote><p>Consider the following L<sup>A</sup>T<sub>E</sub>X source excerpt:</p><pre class="verbatim">You can \raisebox{.6ex}{\em raise} text.
</pre><p>L<sup>A</sup>T<sub>E</sub>X typesets this as follows:
</p><blockquote class="quote">
<img src="manual001.png">
</blockquote><p>Since H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A does not know about <code class="verb">\raisebox</code>,
it incorrectly processes this input. More precisely,
it first prints a warning message:
</p><pre class="verbatim">trouble.tex:34: Unknown macro: \raisebox
</pre><p>
Then, it goes on by translating the arguments of <code class="verb">\raisebox</code> as if
they were normal text. As a
consequence some <code class="verb">.6ex</code> is finally found in the html output:
</p><blockquote class="quote">
You can .6ex<em>raise</em> text.
</blockquote><p>To correct this, you should provide a macro that has more or less the effect of
<code class="verb">\raisebox</code>. It is impossible to write a generic
<code class="verb">\raisebox</code> macro for H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A, because of html limitations.<sup><a id="text2" href="#note2">1</a></sup>
However, in this case, the effect of <code class="verb">\raisebox</code> is to raise the box <em>a little</em>.
Thus, the first, numerical, argument to <code class="verb">\raisebox</code> can be
ignored in a private <code class="verb">\raisebox</code> macro defined in <span class="c017">trouble.hva</span>:
</p><pre class="verbatim">\newcommand{\raisebox}[2]{$^{\mbox{#2}}$}
</pre><p>Now, translating the document yields:
</p><blockquote class="quote">
You can <sup><em>raise</em></sup> text a little.
</blockquote><p>Of course, this will work only when all <code class="verb">\raisebox</code> commands in
the document raise text a little. Consider, the following
example, where text
is both raised a lowered a little:
</p><pre class="verbatim">You can \raisebox{.6ex}{\em raise}
or \raisebox{-.6ex}{\em lower} text.
</pre><p>
Which L<sup>A</sup>T<sub>E</sub>X renders as follows:
</p><blockquote class="quote">
<img src="manual002.png">
</blockquote><p>
Whereas, with the above definition of <code class="verb">\raisebox</code>, H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A produces:
</p><blockquote class="quote">
You can <sup><em>raise</em></sup>
or <sup><em>lower</em></sup> text.
</blockquote><p>A solution is to add a new macro definition in the <code class="verb">trouble.hva</code> file:
</p><pre class="verbatim">\newcommand{\lowerbox}[2]{$_{\mbox{#2}}$}
</pre><p>
Then, <code class="verb">trouble.tex</code> itself has to be modified a little.
</p><pre class="verbatim">You can \raisebox{.6ex}{\em raise}
or \lowerbox{-.6ex}{\em lower} text.
</pre><p>
H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A now produces a satisfying output:
</p><blockquote class="quote">

You can <sup><em>raise</em></sup>
or <sub><em>lower</em></sub> text.
</blockquote><p>Note that, for the document to remain L<sup>A</sup>T<sub>E</sub>X-processable,
it should also contain the following definition for
<code class="verb">\lowerbox</code>:
</p><pre class="verbatim">\newcommand{\lowerbox}[2]{\raisebox{#1}{#2}}
</pre><p>
This definition can safely be placed anywhere in <span class="c017">trouble.tex</span>,
since by H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A semantics for <code class="verb">\newcommand</code> (see
section&#XA0;<a href="manual030.html#usermacro">B.8.1</a>)
the new definition will not overwrite the old one.</p>
<h3 class="subsection" id="sec24">4.2&#X2003;H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A incorrectly interprets a macro</h3>
<p><a id="blob"></a><a id="hevea_default7"></a>
<span class="c027">Note&#XA0;:</span> H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A now renders the <code class="verb">\rule</code> command very similarly to L<sup>A</sup>T<sub>E</sub>X. Hence the forthcomming section is in some sense obsolete. Nevertheless, the technique described is useful and the section is still worth reading.</p><p>Sometimes H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A knows about a macro, but the produced html
does not look good when seen through a browser.
This kind of errors is detected while visually checking the
output.
However, H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A does its best to issue warnings when such situations
are likely to occur.</p><p>Consider, for instance, this definition of <code class="verb">\blob</code> as a small
black square.
</p><pre class="verbatim">\newcommand{\blob}{\rule[.2ex]{1ex}{1ex}}
\blob\ Blob \blob
</pre><p>
Which L<sup>A</sup>T<sub>E</sub>X typesets as follows:
</p><blockquote class="quote">
<img src="manual003.png"></blockquote><p>
H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A always translates <code class="verb">\rule</code> as <code class="verb">&lt;hr&gt;</code>, ignoring size
arguments.
Hence, it produces the following, wrong, output:
</p><blockquote class="quote">

<hr> Blob <hr>
</blockquote><p>We may not be particularily commited to a square blob.
In that case, other small symbols would perfectly do the job
of <code class="verb">\blob</code>, such as a bullet (<code class="verb">\bullet</code>).
Thus, you may choose to give <code class="verb">\blob</code> a definition in
<code class="verb">trouble.hva</code>:
</p><pre class="verbatim">\newcommand{\blob}{\bullet}
</pre><p>
This new definition yields the following, more satisfying output:
</p><blockquote class="quote">&#X2022; Blob &#X2022;
</blockquote><p><a id="square:blob"></a>
<a id="hevea_default8"></a>In case we do want a square blob, there are two alternatives.
We can have L<sup>A</sup>T<sub>E</sub>X typeset some subparts of
the document and then to include them as images, section&#XA0;<a href="manual008.html#imagen">6</a>
explains how to proceed.
We can also find a square blob somewhere in the variety of Unicode
(or do I mean ISO&#XA0;10646?) characters,
and define <code class="verb">\blob</code> as a numerical
character reference. Here, the character <span class="c017">U+02588</span>
seems ok.
</p><pre class="verbatim">\newcommand{\blob}{\@print@u{X2588}}
</pre><blockquote class="quote">&#X2588; Blob &#X2588;
</blockquote><p>
However, beware that not all browsers display all of Unicode&#X2026;</p><p>Finally, users that experience poor rendering of the <code class="verb">\rule</code> command can also upgrade H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A, so as to benefit from the new implementation of this
command:
</p><div class="center">

<div class="c015"><svg height="8px" width="8px"><rect class="rule-rect" height="100%" width="100%"></rect></svg></div> Blob <div class="c015"><svg height="8px" width="8px"><rect class="rule-rect" height="100%" width="100%"></rect></svg></div></div>
<h3 class="subsection" id="sec25">4.3&#X2003;H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A crashes</h3>
<p>H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A failure may have many causes, including a bug.
However, it may also stem from a wrong L<sup>A</sup>T<sub>E</sub>X input.
Thus, this section is to be read before reporting a bug&#X2026;</p>
<h4 class="subsubsection" id="sec26">4.3.1&#X2003;Simple cases: L<sup>A</sup>T<sub>E</sub>X also crashes</h4>
<p>
In the following source, environments are not properly balanced:
</p><pre class="verbatim">\begin{flushright}
\begin{quote}
This is right-flushed quoted text.
\end{flushright}
\end{quote}
</pre><p>
Such a source will make both L<sup>A</sup>T<sub>E</sub>X and H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A choke.
H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A issues the following error message that shows the L<sup>A</sup>T<sub>E</sub>X
environment that is not closed properly:
</p><pre class="verbatim">./trouble.tex:6: Environment nesting error: html: 'DIV' closes 'BLOCKQUOTE'
./trouble.tex:4: Latex environment 'quote' is pending
Adios
</pre><p>
Thus, when H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A crashes, it is a good idea to check that the
input is correct by running L<sup>A</sup>T<sub>E</sub>X on it.</p>
<h4 class="subsubsection" id="sec27">4.3.2&#X2003;Complicated cases</h4>
<p>Unfortunately, H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A may crash on input that does not affect
L<sup>A</sup>T<sub>E</sub>X.
Such errors usually relate to environment or group nesting.</p><p>Consider for instance the following &#X201C;optimized&#X201D; version of a
<code class="verb">quoteright</code> environment:
</p><pre class="verbatim">\newenvironment{quoteright}{\quote\flushright}{\endquote}

\begin{quoteright}
This a right-flushed quotation
\end{quoteright}
</pre><p>The <code class="verb">\quote</code> and <code class="verb">\flushright</code> constructs
are intended to replace
<code class="verb">\begin{quote}</code> and <code class="verb">\begin{flushright}</code>,
while <code class="verb">\endquote</code> stands for <code class="verb">\end{quote}</code>.
Note that the closing <code class="verb">\endflushright</code>
is omitted, since it does nothing.
L<sup>A</sup>T<sub>E</sub>X accepts such an input and produces a right-flushed quotation.</p><p>However, H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A usually translates L<sup>A</sup>T<sub>E</sub>X environments to html
block-level elements and it <em>requires</em>
those elements to be nested properly.
Here, <code class="verb">\quote</code> translates to <code class="verb">&lt;blockquote&gt;</code>,
<code class="verb">\flushright</code> translates to <code class="verb">&lt;div class="flushright"&gt;</code> and
<code class="verb">\endquote</code> translates to <code class="verb">&lt;/blockquote&gt;</code>.
At that point, H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A refuses to generate obviously
non-correct html and it crashes:
</p><pre class="verbatim">Giving up command: \@close
Giving up command: \endquote
Giving up command: \endquoteright
Giving up command: \end
./trouble.tex:7: Environment nesting error: html: 'BLOCKQUOTE' closes 'DIV'
./trouble.tex:5: Latex environment 'quoteright' is pending
Adios
</pre><p>
Also notice that the error message above includes a backtrace showing
the call-chain of commands.</p><p>In this case, the solution is easy: environments must be opened and
closed consistently. L<sup>A</sup>T<sub>E</sub>X style being recommended, one should write:
</p><pre class="verbatim">\newenvironment{quoteright}
  {\begin{quote}\begin{flushright}}
  {\end{flushright}\end{quote}}
</pre><p>
And we get:
</p><blockquote class="quote">

<blockquote class="quote"><div class="flushright">
This is a right-flushed quotation
</div></blockquote>

</blockquote><p>Unclosed L<sup>A</sup>T<sub>E</sub>X groups (<code class="verb">{</code>&#X2026;) are another source
of nuisance to H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A.
Consider the following <span class="c017">horreur.tex</span> file:
</p><pre class="verbatim">\documentclass{article}

\begin{document}
In this sentence, a group is opened now {\em and never closed.
\end{document}
</pre><p>
L<sup>A</sup>T<sub>E</sub>X accepts this file, although it produces a warning:
</p><pre class="verbatim"># latex horreur.tex 
This is TeX, Version 3.14159 (Web2C 7.2)
  ...
(\end occurred inside a group at level 1)
Output written on horreur.dvi (1 page, 280 bytes).

</pre><p>By contrast, running H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A on <span class="c017">horreur.tex</span> yields a fatal error:
</p><pre class="verbatim"># hevea horreur.tex 
Giving up command: \@raise@enddocument
Giving up command: \enddocument
Giving up command: \end
./horreur.tex:4: Environment nesting error: Latex env error: 'document' closes ''
./horreur.tex:3: Latex environment '' is pending
Adios
</pre><p>
Thus, users should close opening braces where it belongs.
Note that H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A error message &#X201C;<span class="c017">Latex environment
&#X2019;</span><span class="c023">env</span><span class="c017">&#X2019; is pending</span>&#X201D; helps a lot in locating
the brace that hurts.</p>
<h4 class="subsubsection" id="sec28">4.3.3&#X2003;Desperate cases</h4>
<p>If H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A crashes on L<sup>A</sup>T<sub>E</sub>X source (not on T<sub>E</sub>X source),
then you may have discovered a bug, or this manual is not as complete
as it should.
In any case, please report to <a href="mailto:Luc.Maranget@inria.fr">Luc.Maranget@inria.fr</a>.</p><p>To be useful, your bug report should include L<sup>A</sup>T<sub>E</sub>X code
that triggers the bug (the shorter, the better) and mention
H<span class="c020"><sup>E</sup></span>V<span class="c020"><sup>E</sup></span>A version number.</p>
<hr class="ffootnoterule"><dl class="thefootnotes"><dt class="dt-thefootnotes"><a id="note2" href="#text2">1</a></dt><dd class="dd-thefootnotes"><div class="footnotetext">Clearly the author of those lines was wrong, as demonstrated <a href="https://github.com/maranget/hevea/pull/45">here</a>.</div></dd></dl>
<hr>
<a href="manual005.html"><img src="previous_motif.svg" alt="Previous"></a>
<a href="manual002.html"><img src="contents_motif.svg" alt="Up"></a>
<a href="manual007.html"><img src="next_motif.svg" alt="Next"></a>
</body>
</html>
